package com.example.practica01g93kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private var btnSaludar: Button? = null
    private  var btnLimpiar: Button? = null
    private  var btnCerrar: Button? = null
    private  var lblSaludar: TextView? = null
    private  var txtSaludo: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Relacionando los objetos con las views
        btnSaludar = findViewById(R.id.btnSaludar) as Button
        btnCerrar = findViewById(R.id.btnCerrar) as Button
        btnLimpiar = findViewById(R.id.btnLimpiar) as Button
        lblSaludar = findViewById(R.id.lblSaludo) as TextView
        txtSaludo = findViewById(R.id.txtSaludo) as EditText

        //Codificando eventos click
        //botón cerrar
        btnCerrar!!.setOnClickListener{
            finish()
        }

        //botón limpiar
        btnLimpiar!!.setOnClickListener{
            lblSaludar!!.text=" "
            txtSaludo!!.setText(" ")
            txtSaludo!!.requestFocus()
        }

        //Botón saludar
        btnSaludar!!.setOnClickListener {

            if (txtSaludo!!.text.toString().isEmpty()){
                Toast.makeText( this@MainActivity, "Favor de ingresar el nombre", Toast.LENGTH_SHORT).show()

            }
            else{
                val saludar = txtSaludo!!.text.toString()
                lblSaludar!!.text = "Hola $saludar,  ¿cómo te encuentras hoy?"
            }

        }

     }


}
